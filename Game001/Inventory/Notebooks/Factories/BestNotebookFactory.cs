﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class GreatNotebookFactory : AbstractFactory
    {

        public GreatNotebookFactory(string Text, Texture2D texture, Vector2 screenPos, string belongs) : base(Text, texture, screenPos, belongs)
        {
            
        }

        public override BaseNotebook createNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs)
        {
            return new GreatNotebook(Text, texture, screenPos, belongs);
        }

    }
}
